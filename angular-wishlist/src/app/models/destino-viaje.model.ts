import {v4 as uuid} from 'uuid';

export class DestinoViaje {
    selected: boolean;
    servicios: string[];
    id = uuid();

    constructor(public nombre: string, public imagenUrl: string) {
        this.servicios = ['Piscina', 'Desayuno'];
    }
    isSelected(): boolean {
        return this.selected;
    }

    setSelected(s: boolean){
        this.selected = s;
    }
}